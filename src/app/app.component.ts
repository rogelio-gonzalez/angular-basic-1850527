import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularBasics1850527';

  alumnosDummy: string[] = ['Barbara', 'Armando'];
  onAddAlumno(name: string){
    this.alumnosDummy.push(name);
  }
}
